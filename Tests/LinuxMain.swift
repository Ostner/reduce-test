import XCTest

import reduceTestTests

var tests = [XCTestCaseEntry]()
tests += reduceTestTests.allTests()
XCTMain(tests)