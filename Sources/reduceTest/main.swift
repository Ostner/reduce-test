import Foundation

let elements = ["> Null", "Eins", "Zwei", "Drei", "> Vier", "Fuenf", "Sechs"]
let openingTag = "<pre>"
let closingTag = "</pre>"
let mark = ">"

func generateString(lines: Int) -> String {
    return (0 ..< lines).map { _ in elements.randomElement()! }.joined(separator: "\n")
}

func measure(_ rounds: Int = 10, _ block: () -> Void) -> TimeInterval {
    var results = [TimeInterval]()
    (0 ..< 100).forEach { _ in
        let start = Date()
        block()
        let end = Date().timeIntervalSince(start)
        results.append(end)
    }
    return results.reduce(0, +) / Double(rounds)
}

func formatWithReduce1(_ string: String) -> String {
    let parts = string.split(separator: "\n")
    var newString = zip(parts, parts.dropFirst() + ["dummy"])
      .reduce(into: [String]()) { acc, parts in
          acc.append(String(parts.0))
          parts.0.hasPrefix(mark) ? acc.append(openingTag)
            : parts.1.hasPrefix(mark) ? acc.append(closingTag)
            : ()
      }
      .joined(separator: "\n")
    newString.append("\n" + closingTag)
    return newString
}

func formatWithReduce2(_ string: String) -> String {
    let parts = string.split(separator: "\n")
    var newString = zip(parts, parts.dropFirst() + ["dummy"])
      .reduce([String]()) { acc, parts in
          var result = acc + [String(parts.0)]
          if parts.0.hasPrefix(mark) {
              result += [openingTag]
          } else if parts.1.hasPrefix(mark) {
              result += [closingTag]
          }
          return result
      }
      .joined(separator: "\n")
    newString.append("\n" + closingTag)
    return newString
}

func formatImparatively(_ string: String) -> String {
    let parts = string.split(separator: "\n")
    var result = [String]()
    for idx in parts.indices {
        result.append(String(parts[idx]))
        if parts[idx].hasPrefix(mark) {
            result.append(openingTag)
        } else {
            let next = parts.index(after: idx)
            if next < parts.endIndex && parts[next].hasPrefix(mark) {
                result.append(closingTag)
            }
        }
    }
    result.append(closingTag)
    return result.joined(separator: "\n")
}

// for size in (4...15).map({ 1 << $0 }) {
//     let string = generateString(lines: size)
//     let resultReduce1 = measure { _ = formatWithReduce1(string) }
//     let resultReduce2 = measure { _ = formatWithReduce2(string) }
//     let resultImparatively = measure { _ = formatImparatively(string) }
//     print("--- \(size)")
//     print("reduce1: \(resultReduce1) | reduce2: \(resultReduce2) | imparatively: \(resultImparatively)")
// }

for size in (10...20).map({ 1 << $0 }) {
    let string = generateString(lines: size)
    let resultReduce1 = measure { _ = formatWithReduce1(string) }
    let resultImparatively = measure { _ = formatImparatively(string) }
    print("--- \(size)")
    print("reduce1: \(resultReduce1) | imparatively: \(resultImparatively)")
}
